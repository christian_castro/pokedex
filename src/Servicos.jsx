import React, { Component } from 'react';
import PokePikachu from '../src/Imagens e Gifs/pikachu.gif'

class Servicos extends Component {
    render() {
        return (
            <div className="pokemom-servicos">
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Pikachu</span></p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Eletric</span> </p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 6kg</span></p>
            <img src={PokePikachu} alt={PokePikachu} />
        </div>
        );
    }
}

export default Servicos;