import React, { Component } from 'react';
import PokeEkans from '../src/Imagens e Gifs/ekans.gif'

class Ekans extends Component {
    render() {
        return (
            <div className="pokemom-ekans">
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Ekans</span></p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Poison</span></p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 6.5kg</span></p>
            <img src={PokeEkans} alt={PokeEkans} />
        </div>
        );
    }
}

export default Ekans;