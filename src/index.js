import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Boss from './Boss.jsx'


ReactDOM.render(
  <Boss />,
  document.getElementById('root')
);

