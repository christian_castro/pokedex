import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './index.css'

class Navegacao extends Component {
    render() {
        return (
            <div className="pokemons">
                <Link className="pokemon-mew" to={"/"}> Mew </Link> 
                <Link className="pokemon-pikachu" to={"/servicos"}> Pikachu </Link> 
                <Link className="pokemon-charmander" to={"/contato"}> Charmander </Link>
                <Link className="pokemon-caterpie" to={"/caterpie"}> Caterpie </Link>
                <Link className="pokemon-ekans" to={"/ekans"}> Ekans </Link>
                <Link className="pokemon-rapidash" to={"/rapidash"}> Rapidash </Link>
                <Link className="pokemon-snorlax" to={"/snorlax"}> Snorlax </Link>
            </div>
        );
    }
}

export default Navegacao;