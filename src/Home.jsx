import React, { Component } from 'react';
import PokeMew from '../src/Imagens e Gifs/pokemew.gif'
import './index.css'

class Home extends Component {
    render() {
        return (
            <div className="pokemom-home">
                <p className="poke-estilo">Nome: <span className="poke-estilo-span">  Mew</span></p>
                <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Phychic</span></p>
                <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 4kg</span></p>
                <img src={PokeMew} alt={PokeMew} />
            </div>
        );
    }
}

export default Home;