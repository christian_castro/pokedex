import React, { Component } from 'react';
import Pokesnorlax from '../src/Imagens e Gifs/snorlax.gif'

class Snorlax extends Component {
    render() {
        return (
            <div className="pokemom-snorlax">
                
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Snorlax</span></p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Normal</span></p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 453.5kg</span></p>
            <img src={Pokesnorlax} alt={Pokesnorlax} />

        </div>
        );
    }
}

export default Snorlax;