import React, { Component } from 'react';

import './index.css'

import Navegacao from './Navegacao'
import Home from './Home'
import Servicos from './Servicos'
import Contatos from './Contatos'
import Caterpie from './Caterpie'
import Ekans from './Ekans'
import Rapidash from './Rapidash'
import Snorlax from './Snorlax'



import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

class Boss extends Component {


    render() {
        return (
            <Router>
                    <div>
                        <h1>POKEDEX</h1>
                      <Navegacao />

                        <div className="pokemons-geral">
                        <Switch><Route exact path="/"> <Home /> </Route></Switch>

                        <Switch><Route exact path="/servicos"> <Servicos /> </Route> </Switch>

                        <Switch><Route exact path="/contato"> <Contatos /> </Route> </Switch>

                        <Switch><Route exact path="/caterpie"> <Caterpie /> </Route> </Switch>

                        <Switch><Route exact path="/ekans"> <Ekans /> </Route> </Switch>

                        <Switch><Route exact path="/rapidash"> <Rapidash /> </Route> </Switch>

                        <Switch><Route exact path="/snorlax"> <Snorlax /> </Route> </Switch>
                        
                    </div>

                    <footer>Desenvolvido por &copy; Christian Silveira</footer>
                </div>
            </Router>
        );
    }
}

export default Boss;