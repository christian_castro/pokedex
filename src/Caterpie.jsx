import React, { Component } from 'react';
import Pokecaterpie from '../src/Imagens e Gifs/caterpie.gif'

class Caterpie extends Component {
    render() {
        return (
            <div className="pokemom-contatos">
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Caterpie</span></p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Bug</span> </p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 2.9kg</span> </p>
            <img src={Pokecaterpie} alt={Pokecaterpie} />

        </div>
        );
    }
}

export default Caterpie;