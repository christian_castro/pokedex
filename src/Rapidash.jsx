import React, { Component } from 'react';
import Pokerapidash from '../src/Imagens e Gifs/rapidash.gif'

class Rapidash extends Component {
    render() {
        return (
            <div className="pokemom-ekans">
                
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Rapidash</span> </p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Fire</span></p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 105.5kg</span></p>
            <img src={Pokerapidash} alt={Pokerapidash} />

        </div>
        );
    }
}

export default Rapidash;