import React, { Component } from 'react';
import Pokecharmander from '../src/Imagens e Gifs/pokecharmander.gif'

class Contatos extends Component {
    render() {
        return (
            <div className="pokemom-contatos">
            <p className="poke-estilo">Nome: <span className="poke-estilo-span"> Charmander</span></p>
            <p className="poke-estilo">Tipo: <span className="poke-estilo-span"> Fire</span></p>
            <p className="poke-estilo">Peso: <span className="poke-estilo-span"> 8.5kg</span></p>
            <img src={Pokecharmander} alt={Pokecharmander} />
        </div>
        );
    }
}

export default Contatos;